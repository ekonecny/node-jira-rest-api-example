# README #

This is an example, written in NodeJS, that illustrates how to perform Jira REST API calls from a Node application.
Specifically, this example creates a new issue and then retrieves it.

##How to use##
* Clone the repository
* Navigate to the directory you cloned
* Perform an "npm install"
* Add your Jira login info to credentials.js (instructions within)
* Run using the command "node test" from the root directory of the project


##Note:##
Kohler's network is currently interfering with the connections from this application to Jira. This must be run on an open network.