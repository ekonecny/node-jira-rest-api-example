//--------------------------------------------------
// API Reference: docs.atlassian.com/jira/REST/cloud
//--------------------------------------------------

var JiraClient = require('jira-connector');
var credentials = require('./credentials');

var jira = new JiraClient( {
    host: 'kohler.atlassian.net',
	 basic_auth: {
        base64: credentials.base64Login
    }
});

//Will hold the key we get back after creating a new ticket
var ticketKey;

//JSON data following the expected schema
//This example only includes this project's required fields
var newTicketData = {
		"fields" : {
			"project" : {
				"key" : "HOST"
			},
			"summary" : "REST API TEST",
			"description" : "Test of automated creation of tickets. If you're reading this, the test is successful",
			"issuetype" : {
				"name" : "Hosting Issue"
			}
		}
	};
/*
//Creates an issue and logs the response
jira.issue.createIssue(newTicketData, function(error, response) {
	console.log((error)?error:response);
	ticketKey = response.key;
});*/

//Retrieves the issue that was just created and logs the issue summary as proof
jira.issue.getIssue({
		issueKey: ticketKey
	}, function(error, issue) {
		console.log((error)?error:"Successfully retrieved issue: "+issue.fields.summary);
});